object Main {
   /* This is my first java program.  
   * This will print 'Hello World' as the output
   */
   def main(args: Array[String]) {
     var fc = new Factor;
     fc.genlist(15)
     var i = fc.factor(6)
     println(i)
     println("FACTORING NEW LIST:::;;;;;;")
     i = fc.factor(12)
     println(i)
   }
}
class Factor() {
  var prime_list: List[Int] = List(2)
  var prime_list_goes_to: Int = 0
  def divides(n:Int,ls:List[Int]) : Boolean = {
    for ( entry <- ls) {
      if (n % entry == 0)  {
        return true
      }
    }
    return false
  }
  def genlist(n:Int) {
    genlist_impl(n,2)
  }
  def genlist_impl(n:Int,i:Int) {
    if (prime_list_goes_to > n ) {
      return
    } else if (i == n) {
      print("prime list(not running because n too small): ")
      for (g <- prime_list) {
        print(s"$g ")
      }

      println()
      prime_list_goes_to = n
      return
    }

    print("PRIME LIST: ")
    for (g <- prime_list) {
      print(s"$g ")
    }

    println()

    //refactor to get rid of else case
    if (divides(i,prime_list) == true ){
      genlist_impl(n,i+1)
    } else {
      prime_list :+= i
      genlist_impl(n,i+1)
    }
    
  }
  def factor(n:Int): List[Int] = {
    genlist(n)
    return factor_i(n,List(1),prime_list)
  }
  def factor_i(n:Int,ls:List[Int],primes:List[Int]) : List[Int] = {
    val entry = primes.head

    println(s"entry:$entry,n:$n")
    print("FACTOR LIST(does not include the last element): ")
    for (g <- ls) {
      print(s"$g ")
    }
    println()
    if (n % entry == 0) {
      if (entry == n) {
        ls:+entry
      } else {
        factor_i(n/entry,ls:+entry,primes)
      }
    } else {
      factor_i(n,ls,primes.tail)
    }
  }
}
